package com.main.project.web;

import com.main.project.entity.Accounts;
import com.main.project.service.AccountService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/account")
@CrossOrigin
public class AccountResource {
    private final AccountService accountService;

    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/all")
    public List<Accounts> getAllAccount(){
        return accountService.findAll();
    }

    @GetMapping("/{identifier}")
    public Accounts getAccount(@PathVariable(value = "identifier") String identifier) {

        return accountService.findByIdentifier(identifier);
    }
}
